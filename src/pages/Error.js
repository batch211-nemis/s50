
//MY ACTIVITY SOLUTION
// import {Row, Col, Button} from 'react-bootstrap';
// import { Link } from 'react-router-dom';

// export default function Error () {

// 	return (
// 		<Row>
// 			<Col>
// 				<h1>404 - Page Not Found!</h1>
// 				<p>The page you are looking for cannot be found.</p>
// 				<Button as={Link} to="/" variant="primary">Go Back to Home</Button>
// 			</Col>
// 		</Row>
// 	)
// }


//SIR ROME ACTIVITY SOLUTION
import Banner from '../components/Banner'

export default function Error () {

	const data = {
		title: "404 - Page Not Found",
		content: "The page you are looking cannot be found",
		destination: "/",
		label: "Go Back to Home"
	}

	return (
		<Banner bannerProp = {data}/>
	)
}