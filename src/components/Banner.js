import {Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner ({bannerProp}) {

	//destructured model of bannerProp
	const { title, content, destination, label } = bannerProp;
	console.log(bannerProp);

	return (
		//text-center to place all text and components in the middle.
		//my = mt and mb = margin top and margin bottom
		//mx = ml and mr = marging left and margin right
		<div className = "text-center my-5">
			<Row>
				<Col>
					<h1>{title}</h1>
					<p>{content}</p>
					<Button as={Link} to={destination} variant="primary">{label}</Button>
				</Col>
			</Row>
		</div>
	)
}

